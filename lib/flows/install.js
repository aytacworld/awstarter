let npm = require('../tasks/npm-install');

function run(config) {
  npm.run(config.npm)
    .subscribe(m => console.log(m));
}

module.exports = {
  run: run
};
