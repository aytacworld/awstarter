let path = require('path');
let cmd = require('child_process');
let parentDir = process.cwd();

function run(packageDir) {
  return Rx.Observable.create(observer => {
    install(packageDir, observer);
  });
}

function install(packages, observer) {
  if (!_.isArray(packages)) {
    packages = [packages];
  }
  let max = packages.length;

  _.each(packages, (p, i) => {
    let text = 'npm install ' + p + ' (' + (i + 1) + '/' + max + '): ';
    observer.next(text + 'starting');
    cmd.execSync('npm install', { cwd: path.resolve(parentDir, p) })
    observer.next(text + 'finished');
  });
  observer.next('npm install: finished');
}

module.exports.run = run;
