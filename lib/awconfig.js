module.exports = {
  install: {
    npm: './',
    bower: null
  },
  build: {
    typescript: null,
    scss: null
  },
  debug: {
    livereload: null
  },
  test: {
    config: null
  },
  deploy: {
    dev: null,
    stage: null,
    prod: null
  }
}
