let awconfigFile = 'awconfig.js';

let path = require('path');
let fs = require('fs');
global._ = require('lodash');
global.Rx = require('@reactivex/rxjs');
let originalConfigFile = require(path.join(__dirname, awconfigFile));
let userConfigFilePath = path.resolve(awconfigFile);
let userConfigFile = {};

if (fs.existsSync(userConfigFilePath)) {
  userConfigFile = require(userConfigFilePath);
}

let configFile = _.assign({}, originalConfigFile, userConfigFile);

let flows = [
  'install',
  'build',
  'debug',
  'test',
  'deploy'
];

function run(arg) {
  if (!_.isNil(arg) || process.argv.length > 2) {
    let flowString = arg || process.argv[2];
    let flowPath;
    if (flows.includes(flowString)) {
      flowPath = path.join(__dirname, 'flows', flowString + '.js');
      let flow = require(flowPath);
      flow.run(configFile[flowString]);
    }
  }
}

module.exports = {
  run: run
};
